//
// Created by David on 05/09/2018.
//

#ifndef NEW_MUONREC_RAWBANK_H
#define NEW_MUONREC_RAWBANK_H

#include <stdint.h>
#include <vector>

// for each raw bank:
// -----------------------------------------------------------------------------
// name                |  type    |  size [bytes]         | array_size
// =============================================================================
// sourceID            | uint32_t | 4                     |
// ------------------------------------------------------------------------------
// bank_data           | char     | variable
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


struct RawBankContent {
    uint32_t sourceID;
    const char* bank_data;

    RawBankContent ();
    explicit RawBankContent (const char *event);
};

RawBankContent::RawBankContent(const char *event) {
    sourceID = *((uint32_t*)event);
    event += sizeof(uint32_t);
    bank_data = event;
}

RawBankContent::RawBankContent() {
    //empty initialization
    sourceID = 0;
}
#endif //NEW_MUONREC_RAWBANK_H
