//
// Created by David on 06/09/2018.
//

#ifndef NEW_MUONREC_RAWBANKFORMAT_H
#define NEW_MUONREC_RAWBANKFORMAT_H

#include "RawBankContent.h"

// Raw bank format:
// -----------------------------------------------------------------------------
// name                |  type    |  size [bytes]         | array_size
// =============================================================================
// Once
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// number_of_rawbanks  | uint32_t | 4
// -----------------------------------------------------------------------------
// raw_bank_offset     | uint32_t | number_of_rawbanks * 4

struct RawBankFormat{
    uint32_t  numRawBanks;
    std::vector<uint32_t> rawBankOffset;
    RawBankContent content;

    RawBankFormat();
    explicit RawBankFormat(const char* event);
};

RawBankFormat::RawBankFormat(){
    numRawBanks = 0;
}

RawBankFormat::RawBankFormat(const char *event) {
    numRawBanks=*((uint32_t*)event);
    event += sizeof(uint32_t);
    for (int i = 0;i < numRawBanks; i++){
        rawBankOffset.push_back(*((uint32_t*)event));
        event += sizeof(uint32_t);
    }
    RawBankContent aux(event);
    content = aux;
}

#endif //NEW_MUONREC_RAWBANKFORMAT_H
