#include <bitset>

#include "RawBankFormat.h"
#include "StatusCode.h"
#include "include/Logger.h"
#include "Kernel/MuonTileID.h"
#include "MuonDAQHelper.h"
#include "MuonDet/DeMuonDetector.h"

//Headers
StatusCode DecodeData(RawBankContent rawBankContent,std::vector<std::pair<LHCb::MuonTileID,unsigned int>>* storage);
//StatusCode checkBankSize(const LHCb::RawBank* rawdata);
StatusCode  decodeTileAndTDCV1(const char* rawdata, uint32_t sourceID, std::vector<std::pair<LHCb::MuonTileID, unsigned int>>* storage);

//Global variables
DeMuonDetector* m_muonDet = nullptr;
std::array<unsigned int, MuonDAQHelper_maxTell1Number*24> m_hitNumInLink;
std::array<unsigned int, MuonDAQHelper_maxTell1Number> m_counter_invalid_hit;
unsigned int m_hitNumInPP[MuonDAQHelper_maxTell1Number*4];
std::array<unsigned int, MuonDAQHelper_maxTell1Number> m_processed_bank;
//LHCb::RawBankReadoutStatus m_status;

int main() {
    //std::vector<RawBank> mb;
    //TODO: get an input file with the raw data and put it into the rawBankFormat and check if it works
    RawBankFormat rawBankFormat; //information is in rawBankFormat.content which is a RawBankContent object
    std::vector<std::pair<LHCb::MuonTileID,unsigned int>> storage;



    //first decode data and insert in buffer
    /*for( const auto& r : rawBankFormat ) {
        StatusCode sc=DecodeData(r);
        if(sc.isFailure())return sc;
    }*/
    StatusCode sc=DecodeData(rawBankFormat.content,&storage);
    if( logger::ll.verbosityLevel >= logger::verbose )
        std::cout<<" the decoding is finished \n";
    //compact data  in one container

    /*for( const auto& r : mb ) {
        unsigned int tell1Number=r.sourceID();
        std::copy(m_storage[tell1Number].begin(),
                  m_storage[tell1Number].end(),
                  std::back_inserter(storage) );
    }*/

    return StatusCode::SUCCESS;
}

StatusCode DecodeData(RawBankContent rawBankContent, std::vector<std::pair<LHCb::MuonTileID, unsigned int>>* storage)
{
    unsigned int tell1Number=rawBankContent.sourceID;
    if(tell1Number>=MuonDAQHelper_maxTell1Number){
        std::cerr<<" in muon data a Tell1 Source ID is gretare than maximum \n";
        return StatusCode::FAILURE;
    }

    StatusCode sc=decodeTileAndTDCV1(rawBankContent.bank_data, rawBankContent.sourceID, storage);


    return sc;
}

StatusCode  decodeTileAndTDCV1(const char* rawdata, uint32_t sourceID, std::vector<std::pair<LHCb::MuonTileID, unsigned int>>* storage){


    //StatusCode sc=checkBankSize(rawdata); //Here rawdata was a RawBank variable
    unsigned int tell1Number=sourceID;
    /*if(sc == StatusCode::FAILURE){
        m_hit_checkSize[tell1Number]++;

        return StatusCode::SUCCESS;
    }*/

    bool print_bank=false;
    bool muon_spec_header=true;

    //printout
    /*if(print_bank) {
        std::cout<<"Tell1 "<<tell1Number<<" "<<"bank length "<<rawdata->size()<<'\n';
        std::for_each( rawdata->begin<unsigned short>(), rawdata->end<unsigned short>(),
                       [&,count_word=0](unsigned short i) mutable {
                           std::cout<<count_word++ <<' '<<(i&(0xFFF))<<' '<<((i>>12)&(0xF))<<'\n';
                       } );
    }*///Always false

    const unsigned short* it=(unsigned short*)rawdata;
    short skip=0;

    m_processed_bank[tell1Number]++;

    if(muon_spec_header) {
        //how many pads ?
        unsigned  short nPads=*it;
        skip=(nPads+3)/2;
        for(int k=0;k<2*skip;k++){
            //if (k==1) fillTell1Header(tell1Number,*it);
            it++;
        }//TODO: No cuadra o se puede mejorar
    }

    std::array<unsigned int,24> hit_link_cnt = { { 0 } };

    for(int i=0;i<4;i++){
        //now go to the single pp counter
        unsigned int pp_cnt=*it++;
        m_hitNumInPP[tell1Number*4+i]=pp_cnt;

        if( logger::ll.verbosityLevel >= logger::verbose )
            std::cout<<" hit in PP "<<pp_cnt<<'\n';

        for (unsigned int loop=0;loop<pp_cnt;++loop) {
            unsigned int add=(*it)&(0x0FFF);
            unsigned int tdc_value=(((*it)&(0xF000))>>12);
            ++it;
            MuonTileID tile=m_muonDet->getDAQInfo()->getADDInTell1(tell1Number,add);
            if( logger::ll.verbosityLevel >= logger::verbose )
                std::cout<<" add "<<add<<' '<<tile <<'\n';
            if(tile.isValid()) {
                if( logger::ll.verbosityLevel >= logger::debug )
                    std::clog<<" valid  add "<<add<<' '<<tile <<'\n';
                storage[tell1Number].emplace_back(tile, tdc_value);
            } else {
                m_counter_invalid_hit[tell1Number]++;
            }
            //update the hitillink counter
            unsigned int link=add/192;
            hit_link_cnt[link]++;
        }
    }
    int active_link_counter=0;
    for(int i=0;i<24;++i){
        if(m_muonDet->getDAQInfo()->getODENumberInLink(tell1Number,i)==0){
            m_hitNumInLink[tell1Number*24+i] = 0;
        }else{
            m_hitNumInLink[tell1Number*24+i] = hit_link_cnt[active_link_counter];
            ++active_link_counter;
        }
    }
    return StatusCode::SUCCESS;
}
