//
// Created by David on 06/09/2018.
//

#ifndef NEW_MUONREC_MUONDAQHELPER_H
#define NEW_MUONREC_MUONDAQHELPER_H

#include <vector>
#include "Kernel/MuonTileID.h"

static const unsigned int MuonDAQHelper_maxTell1Number=14;

class MuonDAQHelper final {
public:
    LHCb::MuonTileID getADDInTell1(unsigned int Tell1_num, long ch) const;
    unsigned int getODENumberInLink(unsigned int Tell1_num,unsigned int Link_num);
private:
    std::vector<long> m_linkInTell1[MuonDAQHelper_maxTell1Number];
    unsigned int m_TotTell1 = 0; // TELL1 counter
    std::vector<LHCb::MuonTileID> m_mapTileInTell1[MuonDAQHelper_maxTell1Number];
};
LHCb::MuonTileID MuonDAQHelper::getADDInTell1(unsigned int Tell1_num, long ch) const {
    LHCb::MuonTileID emptyTile;
    if(Tell1_num<=m_TotTell1){
        if(static_cast<unsigned int> (ch)<(m_mapTileInTell1[Tell1_num]).size()){
            return (m_mapTileInTell1[Tell1_num])[ch];
        }
    }
    return emptyTile;
}

unsigned int MuonDAQHelper::getODENumberInLink(unsigned int Tell1_num,unsigned int Link_num)
{
    size_t s = m_linkInTell1[Tell1_num].size();
    return s == 0 ? 0 :  (m_linkInTell1[Tell1_num])[Link_num];
}


#endif //NEW_MUONREC_MUONDAQHELPER_H
