//
// Created by David on 08/08/2018.
//

#ifndef NEW_MUONREC_STATUSCODE_H
#define NEW_MUONREC_STATUSCODE_H

enum StatusCode {SUCCESS = 1, FAILURE = 0, ERROR = -1};
#endif //NEW_MUONREC_STATUSCODE_H
