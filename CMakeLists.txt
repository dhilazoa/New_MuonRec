cmake_minimum_required(VERSION 3.0)
project(New_MuonRec)

set(CMAKE_CXX_STANDARD 14)
include_directories(Kernel include)

file(GLOB kernel_sources "Kernel/*")
file(GLOB include_sources "include/*")

add_executable(New_MuonRec main.cpp ${kernel_sources} ${include_sources})
