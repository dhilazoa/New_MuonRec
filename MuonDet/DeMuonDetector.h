// ============================================================================
#ifndef MUONDET_DEMUONDETECTOR_H
#define MUONDET_DEMUONDETECTOR_H 1

#include <memory>


#include "../MuonDAQHelper.h"

#include "../StatusCode.h"

using namespace LHCb;

// Forward declarations
class MuonChamberLayout;
class IDataProviderSvc;


/** @class DeMuonDetector DeMuonDetector.h MuonDet/DeMuonDetector.h
 *
 *  Detector element class for the muon system
 *
 *  @author Alessia Satta
 *  @author Alessio Sarti
 *  @date   18/10/2005
 */

static const int CLID_DEMuonDetector = 11009;



class DeMuonDetector {

public:
  /// Constructor
  DeMuonDetector() = default;

  ~DeMuonDetector() = default;

  //Initialize of Detector Element
  StatusCode initialize() ;

  inline MuonDAQHelper* getDAQInfo(){
    return &(m_daqHelper);
  };

private:


  MuonDAQHelper m_daqHelper;
};
#endif    // MUONDET_DEMUONDETECTOR_H
